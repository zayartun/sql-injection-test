You need to create MySQL database and admin table.

<b>Table Structure</b>

CREATE TABLE `admin` (

  `id` int(11) NOT NULL,
  
  `user_name` varchar(255) NOT NULL,
  
  `password` varchar(255) NOT NULL,
  
  `status` int(11) NOT NULL
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

And then, you have to insert some data to this table.

<b>Dumping data to this table</b>

INSERT INTO `admin` (`id`, `user_name`, `password`, `status`) VALUES

(1, 'ayeaye', 'ayeaye123', 0),

(2, 'myamya', 'myamya', 0);

<b>Index for this table</b>

ALTER TABLE `admin`

  ADD PRIMARY KEY (`id`);

<b>AUTO_INCREMENT for this table</b>

ALTER TABLE `admin`

MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

COMMIT;

====================================================================

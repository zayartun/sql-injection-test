<!DOCTYPE html>
<html>
<head>
	<title>SQL Injection Test</title>
	<link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>

	<div id="head-panel">
		<h2>SQL Injection Test</h2>
	</div>

	<div id="login-panel">
		<form action="login-pdo.php" method="post" >
			<h3>Admin Login Panel</h3>

			<label>Username</label>
			<input type="text" id="username" name="username" size="120" placeholder="Enter username here"><br>

			<label>Password</label>
			<input type="password" id="password" name="password" placeholder="Enter password here"><br>

			<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
			<input type="submit" value="Login">
		</form>
	</div>

</body>
</html>
<?php

	session_start();

	include('db_config.php');

	$charset = 'utf8mb4';
	$dsn = "mysql:host=$db_host;dbname=$db_name;charset=$charset";
	$opt = [
		PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
    	PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
    	PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
	];

	$pdo = new PDO($dsn, $db_username, $db_password, $opt);

	$user_name = $_POST['username'];
	$password = $_POST['password'];

	$query = "SELECT * FROM admin WHERE user_name=? AND password=?";
	$stmts = $pdo->prepare($query);
	$stmts->execute([$user_name, $password]);

	$row = $stmts->fetch();
	if ($row) {
		echo('<h1>Login Successful</h1>');
		die('Status : Success');
	} else {
		echo('<h1>Wrong username or password! Try again!!!</h1>');
		die('Status : Failed');
	}

?>